package com.zeros.jsonapiexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter <UserAdapter.UserDataViewHolder> {
    private ArrayList<User> users;
    private Context context;

    //Constructor
    public UserAdapter(ArrayList<User> users, Context context) {
        this.users = users;
        this.context = context;
    }

    //Set layout
    @Override
    public UserDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View userView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list,parent,false);
        return new UserDataViewHolder(userView);
    }

    //set view
    @Override
    public void onBindViewHolder(@NonNull UserDataViewHolder holder, int position) {
        User user = users.get(position);
        Picasso.with(context).load(user.getUrl_avata()).into(holder.imageAvatarUrl);
        holder.tvLoginUser.setText(user.getLogin());
        holder.tvIdUser.setText(user.getId());
    }


    @Override
    public int getItemCount() {
        return users.size();
    }
    public static class UserDataViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageAvatarUrl;
        public TextView tvLoginUser;
        public TextView tvIdUser;
        public UserDataViewHolder(@NonNull View itemView) {
            super(itemView);
            imageAvatarUrl = (ImageView) itemView.findViewById(R.id.image_avatar);
            tvIdUser = (TextView) itemView.findViewById(R.id.id_user);
            tvLoginUser = (TextView) itemView.findViewById(R.id.login_user);

        }
    }
}
