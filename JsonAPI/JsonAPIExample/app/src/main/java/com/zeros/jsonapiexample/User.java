package com.zeros.jsonapiexample;

public class User {
    private String login;
    private int id;
    private String url_avata;

    public User(String login, int id, String url_avata) {
        this.login = login;
        this.id = id;
        this.url_avata = url_avata;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl_avata() {
        return url_avata;
    }

    public void setUrl_avata(String url_avata) {
        this.url_avata = url_avata;
    }
}
