package com.zeros.jsonapiexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Khởi tạo RecyclerView
        final RecyclerView userRecyclerView = (RecyclerView) findViewById(R.id.rv_list_user);
        //set LayoutManager cho RecyclerView
        userRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        //Khởi tạo OkHttpClient
        OkHttpClient client = new OkHttpClient();
        //Khởi tạo Moshi
        Moshi moshi = new Moshi.Builder().build();
        Type userType = Types.newParameterizedType(ArrayList.class,User.class);
        final JsonAdapter<ArrayList<User>> jsonAdapter =moshi.adapter(userType);
        //Gửi request lên sever
        Request request = new Request.Builder().url("https://api.github.com/users").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Error","Network Error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().toString();
                final ArrayList<User> users = jsonAdapter.fromJson(json);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        userRecyclerView.setAdapter(new UserAdapter(users,MainActivity.this));
                    }
                });
            }
        });
    }

}
